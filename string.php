<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        echo "<h5> Bagian 1</h5>";

        $first_sentence = "Hello PHP!" ;
        echo "<p>$first_sentence</p>";
        $panjang_string = strlen($first_sentence);
        $jumlah_kata = str_word_count($first_sentence);
        echo "<p>Panjang String = $panjang_string</p>";
        echo "<p>Jumlah Kata = $jumlah_kata</p>"; // Panjang string 10, jumlah kata: 2
        
        echo "<h5> Bagian 2</h5>";
        $second_sentence = "I'm ready for the challenges";
        echo "<p>$second_sentence</p>";
        $panjang_string = strlen($second_sentence);
        $jumlah_kata = str_word_count($second_sentence);
        echo "<p>Panjang String = $panjang_string</p>";
        echo "<p>Jumlah Kata = $jumlah_kata</p>"; // Panjang string: 28,  jumlah kata: 5
        
        echo "<h3> Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: " . substr($string2, 2, 4) . "<br>" ; 
        echo "Kata Ketiga: " . substr($string2, 7, 3) . "<br>" ; ;

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but sexy!";
        echo "String: \"$string3\"<br> ";
        echo "<label>" . str_replace("sexy!", "awesome", $string3); "</label><br>"; 
        // OUTPUT : "PHP is old but awesome"

    ?>
</body>
</html>